package ru.tsc.ichaplygina.taskmanager.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

public class DomainSaveYAMLFasterXMLCommand extends AbstractDomainCommand {

    @NotNull
    public final static String NAME = "save yaml fasterxml";

    @NotNull
    public final static String DESCRIPTION = "save projects, tasks and users to yaml file using fasterxml";

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getAdminEndpoint().saveYAMLFasterXML(getSession());
    }

}
