package ru.tsc.ichaplygina.taskmanager.exception.incorrect;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public final class IncorrectCredentialsException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Error! Incorrect credentials.";

    public IncorrectCredentialsException() {
        super(MESSAGE);
    }

}
