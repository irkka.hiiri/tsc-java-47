package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IUserService;
import ru.tsc.ichaplygina.taskmanager.model.User;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.empty.EmailEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWithEmailException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWithLoginException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IncorrectCredentialsException;
import ru.tsc.ichaplygina.taskmanager.marker.DatabaseCategory;
import ru.tsc.ichaplygina.taskmanager.service.model.UserService;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private List<User> userList;
    @NotNull
    private IUserService userService;

    @After
    public void clean() {
        for (@NotNull final User testUser : userService.findAll()) {
            if (testUser.getLogin().equals("testUser") || testUser.getLogin().equals("testAdmin") || testUser.getLogin().equals("new_login")
                    || testUser.getLogin().equals("mouse") || testUser.getLogin().equals("cat") || testUser.getLogin().equals("dog"))
                userService.removeById(testUser.getId());
        }
    }

    @BeforeClass
    public static void initConnectionService() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initTest() {
        userService = new UserService(connectionService, propertyService);
        @NotNull final User testAdmin = new User("testAdmin", "testAdmin", "testAdmin@testAdmin", "A.", "D.", "Min", Role.ADMIN);
        @NotNull final User testUser = new User("testUser", "testUser", "testUser@testUser", "U.", "S.", "Er", Role.USER);
        @NotNull final User cat = new User("cat", "cat", "cat@cat", "C.", "A.", "T", Role.USER);
        @NotNull final User mouse = new User("mouse", "mouse", "mouse@mouse", "M.", "O.", "Use", Role.USER);
        userList = new ArrayList<>();
        userList.add(testAdmin);
        userList.add(testUser);
        userList.add(cat);
        userList.add(mouse);
        userService.add(testAdmin);
        userService.add(testUser);
        userService.add(cat);
        userService.add(mouse);
        userService.lockByLogin("mouse");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testAdd() {
        Assert.assertEquals(4, userService.getSize());
        userService.add("dog", "dog", "dog@dog", Role.USER, "D.", "O.", "G");
        Assert.assertEquals(5, userService.getSize());
        userService.removeByLogin("dog");
    }

    @Test(expected = UserExistsWithEmailException.class)
    @Category(DatabaseCategory.class)
    public void testAddEmailExists() {
        userService.add("dog", "dog", "cat@cat", Role.USER, "D.", "O.", "G");
    }

    @Test(expected = EmailEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testAddEmptyEmail() {
        userService.add("dog", "dog", "", Role.USER, "D.", "O.", "G");
    }

    @Test(expected = LoginEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testAddEmptyLogin() {
        userService.add("", "dog", "dog@dog", Role.USER, "D.", "O.", "G");
    }

    @Test(expected = PasswordEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testAddEmptyPassword() {
        userService.add("dog", "", "dog@dog", Role.USER, "D.", "O.", "G");
    }

    @Test(expected = UserExistsWithLoginException.class)
    @Category(DatabaseCategory.class)
    public void testAddLoginExists() {
        userService.add("cat", "dog", "dog@dog", Role.USER, "D.", "O.", "G");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testClear() {
        userService.clear();
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAll() {
        Assert.assertEquals(4, userService.findAll().size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindById() {
        @NotNull final String testUserId = userService.findAll().get(0).getId();
        Assert.assertNotNull(userService.findById(testUserId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByLogin() {
        Assert.assertNotNull(userService.findByLogin("testAdmin"));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByLoginForAuthorization() {
        Assert.assertNotNull(userService.findByLoginForAuthorization("testAdmin"));
    }

    @Test(expected = IncorrectCredentialsException.class)
    @Category(DatabaseCategory.class)
    public void testFindByLoginForAuthorizationUserNotFound() {
        userService.findByLoginForAuthorization("dog");
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testFindByLoginUserNotFound() {
        userService.findByLogin("dog");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetSize() {
        Assert.assertEquals(4, userService.getSize());
        userService.add(new User("dog", "dog", "dog@dog", "M.", "O.", "Use", Role.USER));
        Assert.assertEquals(5, userService.getSize());
        userService.clear();
        Assert.assertEquals(0, userService.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsEmpty() {
        Assert.assertFalse(userService.isEmpty());
        userService.clear();
        Assert.assertTrue(userService.isEmpty());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsPrivilegedUser() {
        Assert.assertTrue(userService.isPrivilegedUser(userList.get(0).getId()));
        Assert.assertFalse(userService.isPrivilegedUser(userList.get(1).getId()));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testIsPrivilegedUserNotFound() {
        userService.isPrivilegedUser("unknown id");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testLockById() {
        @NotNull final String id = userList.get(0).getId();
        Assert.assertTrue(userService.lockById(id));
        Assert.assertTrue(userService.findById(id).isLocked());
    }

    @Test(expected = IdEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testLockByIdEmptyId() {
        userService.lockById("");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testLockByIdUserAlreadyLocked() {
        Assert.assertFalse(userService.lockById(userList.get(3).getId()));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testLockByIdUserNotFound() {
        userService.lockById("unknown id");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testLockByLogin() {
        Assert.assertTrue(userService.lockByLogin("testAdmin"));
        Assert.assertTrue(userService.findByLogin("testAdmin").isLocked());
    }

    @Test(expected = LoginEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testLockByLoginEmptyLogin() {
        userService.lockByLogin("");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testLockByLoginUserAlreadyLocked() {
        Assert.assertFalse(userService.lockByLogin("mouse"));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testLockByLoginUserNotFound() {
        userService.lockByLogin("dog");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testRemoveByLogin() {
        Assert.assertNotNull(userService.removeByLogin("mouse"));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testRemoveByLoginNotFound() {
        userService.removeByLogin("dog");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testSetPassword() {
        @NotNull final String oldHash = userList.get(0).getPasswordHash();
        userService.setPassword("testAdmin", "123");
        Assert.assertNotEquals(oldHash, userService.findByLogin("testAdmin").getPasswordHash());
    }

    @Test(expected = LoginEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testSetPasswordEmptyLogin() {
        userService.setPassword("", "password");
    }

    @Test(expected = PasswordEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testSetPasswordEmptyPassword() {
        userService.setPassword("login", "");
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testSetPasswordUserNotFound() {
        userService.setPassword("login", "password");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testSetRole() {
        userService.setRole("testUser", Role.ADMIN);
        Assert.assertTrue(userService.isPrivilegedUser(userService.findByLogin("testUser").getId()));
    }

    @Test(expected = LoginEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testSetRoleEmptyLogin() {
        userService.setRole("", Role.ADMIN);
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testSetRoleUserNotFound() {
        userService.setRole("odmin", Role.ADMIN);
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUnlockById() {
        @NotNull final String id = userList.get(3).getId();
        Assert.assertTrue(userService.unlockById(id));
        Assert.assertFalse(userService.findById(id).isLocked());
    }

    @Test(expected = IdEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUnlockByIdEmptyId() {
        userService.unlockById("");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUnlockByIdUserAlreadyUnlocked() {
        Assert.assertFalse(userService.unlockById(userList.get(0).getId()));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testUnlockByIdUserNotFound() {
        userService.unlockById("unknown id");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUnlockByLogin() {
        Assert.assertTrue(userService.unlockByLogin("mouse"));
        Assert.assertFalse(userService.findByLogin("mouse").isLocked());
    }

    @Test(expected = LoginEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUnlockByLoginEmptyLogin() {
        userService.unlockByLogin("");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUnlockByLoginUserAlreadyUnlocked() {
        Assert.assertFalse(userService.unlockByLogin("testAdmin"));
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testUnlockByLoginUserNotFound() {
        userService.unlockByLogin("dog");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUpdateById() {
        @NotNull final User testUserExpected = userList.get(0);
        @NotNull final String testUserId = testUserExpected.getId();
        @NotNull final String newLogin = "new_login";
        @NotNull final String newPassword = "new_password";
        @NotNull final String newEmail = "new_email1";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        testUserExpected.setLogin(newLogin);
        testUserExpected.setPasswordHash(newPassword);
        testUserExpected.setEmail(newEmail);
        testUserExpected.setRole(newRole);
        testUserExpected.setFirstName(newFirstName);
        testUserExpected.setMiddleName(newMiddleName);
        testUserExpected.setLastName(newLastName);
        @Nullable final User testUserActual = userService.updateById(testUserId, newLogin, newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
        Assert.assertEquals(testUserExpected.getId(), testUserActual.getId());
        Assert.assertEquals(testUserExpected.getLogin(), testUserActual.getLogin());
        Assert.assertEquals(testUserExpected.getPasswordHash(), testUserActual.getPasswordHash());
        Assert.assertEquals(testUserExpected.getEmail(), testUserActual.getEmail());
        Assert.assertEquals(testUserExpected.getRole(), testUserActual.getRole());
        Assert.assertEquals(testUserExpected.getFirstName(), testUserActual.getFirstName());
        Assert.assertEquals(testUserExpected.getMiddleName(), testUserActual.getMiddleName());
        Assert.assertEquals(testUserExpected.getLastName(), testUserActual.getLastName());
    }

    @Test(expected = LoginEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByIdEmptyLogin() {
        @NotNull final User testUserExpected = userList.get(0);
        @NotNull final String testUserId = testUserExpected.getId();
        @NotNull final String newLogin = "";
        @NotNull final String newPassword = "new_password";
        @NotNull final String newEmail = "new_email2";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateById(testUserId, newLogin, newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test(expected = PasswordEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByIdEmptyPassword() {
        @NotNull final User testUserExpected = userList.get(0);
        @NotNull final String testUserId = testUserExpected.getId();
        @NotNull final String newLogin = "login";
        @NotNull final String newPassword = "";
        @NotNull final String newEmail = "new_email3";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateById(testUserId, newLogin, newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test(expected = UserExistsWithEmailException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByIdUserExistsWithEmail() {
        @NotNull final User testUserExpected = userList.get(0);
        @NotNull final String testUserId = testUserExpected.getId();
        @NotNull final String newLogin = "login";
        @NotNull final String newPassword = "password";
        @NotNull final String newEmail = "testUser@testUser";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateById(testUserId, newLogin, newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test(expected = UserExistsWithLoginException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByIdUserExistsWithLogin() {
        @NotNull final User testUserExpected = userList.get(0);
        @NotNull final String testUserId = testUserExpected.getId();
        @NotNull final String newLogin = "testUser";
        @NotNull final String newPassword = "password";
        @NotNull final String newEmail = "email";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateById(testUserId, newLogin, newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByIdUserNotFound() {
        @NotNull final String testUserId = "123";
        @NotNull final String newLogin = "login";
        @NotNull final String newPassword = "password";
        @NotNull final String newEmail = "email";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateById(testUserId, newLogin, newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testUpdateByLogin() {
        @NotNull final User testUserExpected = userList.get(0);
        @NotNull final String newPassword = "new_password";
        @NotNull final String newEmail = "new_email4";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        testUserExpected.setPasswordHash(newPassword);
        testUserExpected.setEmail(newEmail);
        testUserExpected.setRole(newRole);
        testUserExpected.setFirstName(newFirstName);
        testUserExpected.setMiddleName(newMiddleName);
        testUserExpected.setLastName(newLastName);
        @Nullable final User testUserActual = userService.updateByLogin("testAdmin", newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
        Assert.assertEquals(testUserExpected.getId(), testUserActual.getId());
        Assert.assertEquals(testUserExpected.getLogin(), testUserActual.getLogin());
        Assert.assertEquals(testUserExpected.getPasswordHash(), testUserActual.getPasswordHash());
        Assert.assertEquals(testUserExpected.getEmail(), testUserActual.getEmail());
        Assert.assertEquals(testUserExpected.getRole(), testUserActual.getRole());
        Assert.assertEquals(testUserExpected.getFirstName(), testUserActual.getFirstName());
        Assert.assertEquals(testUserExpected.getMiddleName(), testUserActual.getMiddleName());
        Assert.assertEquals(testUserExpected.getLastName(), testUserActual.getLastName());
    }

    @Test(expected = LoginEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByLoginEmptyLogin() {
        @NotNull final User testUserExpected = userList.get(0);
        @NotNull final String newPassword = "new_password";
        @NotNull final String newEmail = "new_email5";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateByLogin("", newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test(expected = PasswordEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByLoginEmptyPassword() {
        @NotNull final User testUserExpected = userList.get(0);
        @NotNull final String newPassword = "";
        @NotNull final String newEmail = "new_email6";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateByLogin("testAdmin", newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test(expected = UserExistsWithEmailException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByLoginUserExistsWithEmail() {
        @NotNull final String newPassword = "password";
        @NotNull final String newEmail = "testAdmin@testAdmin";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateByLogin("cat", newPassword, newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

    @Test(expected = UserNotFoundException.class)
    @Category(DatabaseCategory.class)
    public void testUpdateByLoginUserNotFound() {
        @NotNull final String newPassword = "password";
        @NotNull final String newEmail = "testAdmin@testAdmin";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userService.updateByLogin("vasya", newPassword,
                newEmail, newRole, newFirstName, newMiddleName, newLastName);
    }

}
