package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.ISessionService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IUserService;
import ru.tsc.ichaplygina.taskmanager.model.Session;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IncorrectCredentialsException;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedException;
import ru.tsc.ichaplygina.taskmanager.marker.DatabaseCategory;
import ru.tsc.ichaplygina.taskmanager.service.model.SessionService;
import ru.tsc.ichaplygina.taskmanager.service.model.UserService;

import java.util.ArrayList;
import java.util.List;

public class SessionServiceTest {

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private static IConnectionService connectionService;

    @NotNull
    private List<Session> sessionList;
    @NotNull
    private ISessionService sessionService;
    @NotNull
    private IUserService userService;

    @After
    public void clean() {
        userService.removeByLogin("testUser");
        userService.removeByLogin("testAdmin");
        userService.removeByLogin("mouse");
        userService.removeByLogin("cat");
        sessionService.clear();
    }

    @BeforeClass
    public static void initConnectionService() {
        propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initTest() {
        userService = new UserService(connectionService, propertyService);
        userService.add("testAdmin", "testAdmin", "testAdmin@testAdmin", Role.ADMIN, "A.", "D.", "Min");
        userService.add("testUser", "testUser", "testUser@testUser", Role.USER, "U.", "S.", "Er");
        userService.add("cat", "cat", "cat@cat", Role.USER, "C.", "A.", "T");
        userService.add("mouse", "mouse", "mouse@mouse", Role.USER, "M.", "O.", "Use");
        userService.lockByLogin("mouse");
        sessionService = new SessionService(connectionService, propertyService, userService);
        sessionList = new ArrayList<>();
        @NotNull final Session testAdminSession = new Session();
        testAdminSession.setUser(userService.findByLogin("testAdmin"));
        @NotNull final Session testUserSession = new Session();
        testUserSession.setUser(userService.findByLogin("testUser"));
        sessionService.add(testAdminSession);
        sessionList.add(testAdminSession);
        sessionService.add(testUserSession);
        sessionList.add(testUserSession);
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testAdd() {
        Assert.assertEquals(2, sessionService.getSize());
        @NotNull Session session = new Session(userService.findByLogin("testUser"));
        sessionService.add(session);
        Assert.assertEquals(3, sessionService.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testAddAll() {
        @NotNull List<Session> sessionList = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            sessionList.add(new Session(userService.findByLogin("testUser")));
        }
        sessionService.addAll(sessionList);
        Assert.assertEquals(12, sessionService.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testClear() {
        sessionService.clear();
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testCloseSession() {
        @NotNull final Session session = sessionService.findAll().get(0);
        sessionService.closeSession(session);
        Assert.assertEquals(1, sessionService.getSize());
        Assert.assertNull(sessionService.findById(session.getId()));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAll() {
        @NotNull List<Session> sessionList = sessionService.findAll();
        Assert.assertEquals(2, sessionList.size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindById() {
        @NotNull final String sessionId = sessionService.findAll().get(0).getId();
        Assert.assertNotNull(sessionService.findById(sessionId));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetSize() {
        Assert.assertEquals(2, sessionService.getSize());
        sessionService.add(new Session(userService.findByLogin("testUser")));
        Assert.assertEquals(3, sessionService.getSize());
        sessionService.clear();
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsEmpty() {
        Assert.assertFalse(sessionService.isEmpty());
        sessionService.clear();
        Assert.assertTrue(sessionService.isEmpty());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testOpenSession() {
        @NotNull final Session session = sessionService.openSession("cat", "cat");
        Assert.assertEquals(3, sessionService.getSize());
        Assert.assertNotNull(session.getSignature());
    }

    @Test(expected = LoginEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testOpenSessionEmptyLogin() {
        sessionService.openSession("", "");
    }

    @Test(expected = PasswordEmptyException.class)
    @Category(DatabaseCategory.class)
    public void testOpenSessionEmptyPassword() {
        sessionService.openSession("cat", "");
    }

    @Test(expected = IncorrectCredentialsException.class)
    @Category(DatabaseCategory.class)
    public void testOpenSessionIncorrectLogin() {
        sessionService.openSession("dog", "dog");
    }

    @Test(expected = IncorrectCredentialsException.class)
    @Category(DatabaseCategory.class)
    public void testOpenSessionIncorrectPassword() {
        sessionService.openSession("cat", "dog");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(DatabaseCategory.class)
    public void testOpenSessionLockedUser() {
        sessionService.openSession("mouse", "mouse");
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testValidatePrivileges() {
        sessionService.validatePrivileges(userService.findByLogin("testAdmin").getId());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(DatabaseCategory.class)
    public void testValidatePrivilegesAccessDenied() {
        sessionService.validatePrivileges(userService.findByLogin("testUser").getId());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testValidateSession() {
        @NotNull final Session session = sessionService.openSession("cat", "cat");
        sessionService.validateSession(session);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(DatabaseCategory.class)
    public void testValidateSessionBadSignature() {
        @NotNull Session session = new Session();
        session.setId(sessionList.get(0).getId());
        session.setUser(userService.findByLogin("testAdmin"));
        session.setTimeStamp(sessionList.get(0).getTimeStamp());
        session.setSignature("lalala");
        sessionService.validateSession(session);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(DatabaseCategory.class)
    public void testValidateSessionNoSignature() {
        sessionService.validateSession(new Session());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(DatabaseCategory.class)
    public void testValidateSessionNotExist() {
        @NotNull Session session = sessionService.openSession("cat", "cat");
        sessionService.removeById(session.getId());
        sessionService.validateSession(session);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(DatabaseCategory.class)
    public void testValidateSessionNull() {
        sessionService.validateSession(null);
    }

}
